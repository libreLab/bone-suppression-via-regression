In field of medical image processing many structure suppression technique had 


been proposed. In example, subspace filtering of elongated translucent bony 


structures(Laurens Hogeweg, 7 November 2005)  this method provide grate bone  


suppression quality via signal processing, unfortunately deal only with  


elongated structures and need previously segmented radiographs. Another 


example is a pixel-wise regression performed by k nearest neighbours and  


dimensionality reduction based on principal component analysis (M. Loog,  


7 November 2005), this method from the latter paper was implemented in following  


work and will serve as a baseline. The the main idea of method in this paper is  


to ensure justification for chosen set of features, such as filter's kernels,  


via unsupervised deep learning methods and provide solution that will better deal  


with cross datasets evaluation and tiny size of training set. That part was  


omitted in work of (M. Loog, 7 November 2005). 

